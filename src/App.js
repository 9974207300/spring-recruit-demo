import React, { Component } from 'react';
import Board from 'react-trello';
import './App.css';

const CustomCard = props => {
  return (
    <div style={{ backgroundColor: props.cardColor, padding: 6 }}>
      <header
        style={{
          borderBottom: '1px solid #eee',
          paddingBottom: 6,
          marginBottom: 10,
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between'
        }}>
        <div style={{ fontSize: 14, fontWeight: 'bold' }}>{props.title}</div>
      </header>
      <div style={{ fontSize: 12, color: '#BD3B36' }}>
        <div style={{ color: '#4C4C4C', fontWeight: 'bold' }}>{props.label}</div>
        <div style={{ padding: '5px 0px' }}>
          <i>{props.description}</i>
        </div>
      </div>
    </div>
  )
}

const CustomLaneHeader = props => {
  return (
    <div>
      <header
        style={{
          borderBottom: '2px solid #c5c5c5',
          paddingBottom: 6,
          marginBottom: 10,
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between'
        }}>
        <div style={{fontSize: 14, fontWeight: 'bold'}}>{props.title}</div>
      </header>
    </div>
  )
}


class App extends Component {
  state = {
    data: {
      lanes: [
        {
          id: 'lane1',
          title: 'Planned',
          cards: [
            {
              id: 'Card1',
              title: 'John Smith',
              label: 'SMS received at 12:13pm today',
              description: 'Thanks. Please schedule me for an estimate on Monday.',
              metadata: {id: 'Card1'}
            },
            {
              id: 'Card2',
              title: 'Card Weathers',
              label: 'Email received at 1:14pm',
              description: 'Is the estimate free, and can someone call me soon?',
              metadata: {id: 'Card1'}
            }
          ]
        },
        {
          id: 'lane2',
          title: 'Work In Progress',
          cards: [
            {
              id: 'Card3',
              title: 'Michael Caine',
              label: 'Email received at 4:23pm today',
              description: 'You are welcome. Interested in doing business with you again',
              metadata: {id: 'Card1'}
            }
          ]
        }
    ]
    }
  }

  componentDidMount() {
    const data = localStorage.getItem('data');
    if (data) {
      this.setState({ data: JSON.parse(data) });
    }
  }

  addCard = (card, laneId) => {
    const { data } = this.state;
    data.lanes = data.lanes.map(lane => {
      if (lane.id === laneId) {
        lane.cards.push(card);
      }

      return lane;
    });

    localStorage.setItem('data', JSON.stringify(data));
    this.setState({ data });
  };

  addLane = (lane) => {
    const { data } = this.state;
    data.lanes.push({ id: new Date().valueOf().toString(), cards: [], ...lane });
    localStorage.setItem('data', JSON.stringify(data));
    this.setState({ data });
  }

  deleteCard = (cardId, laneId) => {
    const { data } = this.state;
    data.lanes = data.lanes.map(lane => {
      if (lane.id === laneId) {
        lane.cards = lane.cards.filter(card => card.id !== cardId);
      }
      return lane;
    });

    localStorage.setItem('data', JSON.stringify(data));
    this.setState({ data });
  }

  render() {
    return (
      <Board data={this.state.data}
        draggable
        editable
        canAddLanes
        customCardLayout
        onCardAdd={this.addCard}
        onLaneAdd={this.addLane}
        onCardDelete={this.deleteCard}
        customLaneHeader={<CustomLaneHeader />}
      >
        <CustomCard />
      </Board>
    );
  }
}

export default App;
